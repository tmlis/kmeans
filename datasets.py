import numpy as np
import pandas as pd


def load2darray():
    """returns 2-D array."""
    tbl = [np.array(np.random.normal(0, 0.2, 100)),
           np.array(np.random.normal(0, 0.2, 100))]

    return np.transpose(tbl)


def load3darray():
    """returns 3-D array."""
    tbl = [np.array(np.random.normal(0, 0.2, 100)),
           np.array(np.random.normal(0, 0.2, 100)),
           np.array(np.random.normal(0, 0.2, 100))]

    return np.transpose(tbl)


def loadIris():
    """returns iris dataset"""
    return pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data',
                       header=None)
