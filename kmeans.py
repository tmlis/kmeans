from random import randint
import numpy as np


def kmeans(data, K):
    """
    idx, centers = kmeans (data, K)

    :param data: NxD table data
    :param K: number of clusters to be found

    :return: idx: Nx1 vector whose i-th element is the class to which row i data is assigned
    :return: centers: A KxD array whose i-th row is the centroid of cluster i.
    """
    idx = np.zeros((data.shape[0], 1), dtype=int)
    centers = np.zeros((K, data.shape[1]))  # cluster centers;  A KxD array whose i-th row is the centroid of cluster i.
    centers_prev = centers
    noiter = 0  # number of iterations
    if K > data.shape[0]: return idx, centers

    # 1  arbitrarily choose k objects from D as the initial cluster centers
    indices = np.random.choice(data.shape[0], K, replace=False)
    centers = data[indices]
    # 2  repeat # 5  until no change
    while not np.array_equiv(centers, centers_prev):
        noiter += 1
        centers_prev = centers.copy()
        #       3  (re)assign each object to the cluster to which the object is the most similar
        _assignToCluster(data, centers, idx, K)
        #       4  update the cluster means
        calculateClusterMeans(data, centers, idx)
    print(noiter)
    return idx, centers

# TODO eliminate this function
def mindistid(vect):
    min = vect[0]
    minid = 0
    for id, val in enumerate(vect):
        if val < min:
            min = val
            minid = id
    return minid


def _assignToCluster(data, centers, idx, k):
    dist = np.zeros(
        (data.shape[0], k))  # An NxK matrix whose ij-th element is the distance from sample i to centroid j.
    for clid, clval in enumerate(centers):  # for each centroid
        for pid, pval in enumerate(data):  # for each object
            dist[pid][clid] = ((pval - clval) ** 2).sum(axis=0)  # compute Euclidean distance dist(object,centroid)

    for did, dval in enumerate(dist):  # for each in distance array
        idx[did] = mindistid(dval)  # assign object to the cluster


def calculateClusterMeans(data, centers, idx):
    sums = np.zeros(centers.shape)
    elements = sums.copy()
    for clid, clval in enumerate(idx):  # for each object
        sums[int(clval), :] += data[clid, :]
        elements[int(clval)] += 1
    for i in range(centers.shape[0]):
        centers[i, :] = sums[i, :] / elements[i]  # update cluster means

