from kmeans import kmeans
import numpy as np
from plots import plotKmeans
from datasets import load2darray, load3darray, loadIris


def findMissingInts(array, min, max):
    """returns list of missing ints from range <min, max> in array"""
    missing = []
    correctarr = [i for i in range(min, max+1, 1)]
    sortedarr = sorted(array)
    for no, val in enumerate(correctarr):
        if val not in sortedarr:
            missing.append(val)
    return missing


def analyseIris():
    """returns clustering classification quality [%]"""
    tbl = loadIris()
    real_idx = tbl.iloc[:, [4]].values
    tbl = tbl.iloc[:, [0, 1, 2, 3]].values
    import time
    start = time.time()
    result, centers = kmeans(tbl, 3)
    evaltime = time.time() - start
    # check error rate
    # assign cluster id to a flower, by majority of cluster ids
    counts = []
    counts.append(np.bincount(result[0:50, 0]))
    counts.append(np.bincount(result[50:100, 0]))
    counts.append(np.bincount(result[100:150, 0]))
    print(counts)
    best_cluster = [-1, -1, -1]
    # tests
    # counts = [np.array([26, 0, 24]), np.array([3, 47]), np.array([0, 50])]  #result is 0 2 1
    # counts = [np.array([0,26,24]), np.array([47,3]), np.array([50])]
    while -1 in best_cluster:
        bestflower = -1  # which flower
        bestid = -1  # which cluster
        bestval = -1  # no of occurrences of a cluster in a flower
        for fid, fval in enumerate(counts):  # for each flower
            for clval, count in enumerate(fval):
                if count > bestval and clval not in best_cluster and (best_cluster[fid] == -1):
                    bestflower = fid
                    bestid = clval
                    bestval = count
        if(bestflower == -1):

            for no, val in enumerate(best_cluster):
                if val == -1:
                    bestflower = no
                    bestid = findMissingInts(best_cluster, 0, 2)[0]
        best_cluster[bestflower] = bestid
    print(best_cluster)

    flowers_dict = {'Iris-setosa': best_cluster[0],
                    'Iris-versicolor': best_cluster[1],
                    'Iris-virginica': best_cluster[2]}
    for no, val in enumerate(real_idx):
        real_idx[no] = flowers_dict[val[0]]

    miscls = np.sum(np.where(result == real_idx, 0, 1))

    # plotKmeans(tbl[:, [0, 1]], result, centers)
    # plotKmeans(tbl[:, [0, 1]], real_idx, centers)

    return (result.shape[0]-miscls)/result.shape[0]*100, evaltime


class TestSolution:
    def __init__(self):
        pass

    def irisTest(self, noiters):
        irisTestDict = {'quality': [], 'evaltime': []}
        for i in range(noiters):
            quality, evaltime = analyseIris()
            irisTestDict['quality'].append(quality)
            irisTestDict['evaltime'].append(evaltime)

        irisTestDict['avgquality'] = sum(irisTestDict['quality']) / len(irisTestDict['quality'])
        irisTestDict['minquality'] = min(irisTestDict['quality'])
        irisTestDict['maxquality'] = max(irisTestDict['quality'])
        irisTestDict['avgevaltime'] = sum(irisTestDict['evaltime']) / len(irisTestDict['evaltime'])

        return irisTestDict

    def d2Test(self):
        tbl = load2darray()
        result, centers = kmeans(tbl, 3)
        plotKmeans(tbl, result, centers)

    def d3Test(self):
        tbl = load3darray()
        result, centers = kmeans(tbl, 3)
        plotKmeans(tbl, result, centers)


def main():
    tester = TestSolution()

    # 1. 2-dimensional array plot
    tester.d2Test()

    # 2. 3-dimensional array plot
    tester.d3Test()

    # 3. testing loop for iris clustering classification
    retries = 100
    icc = tester.irisTest(retries)
    print(icc['avgquality'], icc['minquality'], icc['maxquality'], icc['avgevaltime'])


if __name__ == '__main__':
    main()
