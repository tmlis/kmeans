import matplotlib.pyplot as plt
import numpy as np


def plotKmeans(data, idx, centers):
    """
    shows plotted data according to a given idx shape.

    :param data: NxD table data
    :param idx: Nx1 vector whose i-th element is the class to which row i data is assigned
    :param centers: A KxD array whose i-th row is the centroid of cluster i.
    """
    # if data contains more dimensions than 3, visualisation is not possible
    size = data.shape[1]
    if size == 1:
        for clval in np.unique(idx):
            plt.scatter(data[[no for no, val in enumerate(idx) if val == clval], 0],
                        data[[no for no, val in enumerate(idx) if val == clval], 0], marker='^')
        # plot centers
        for clid in range(centers.shape[0]):
            plt.scatter(centers[clid, 0], centers[clid, 0], c='r', marker='o')
        plt.xlabel('Feature 1')
        plt.ylabel('Feature 1')

        plt.show()
    elif size == 2:
        for clid in np.unique(idx):
            plt.scatter(data[[no for no, i in enumerate(idx) if i == clid], 0],
                        data[[no for no, i in enumerate(idx) if i == clid], 1], marker='^')
        # plot centers
        for clid in range(centers.shape[0]):
            plt.scatter(centers[clid, 0], centers[clid, 1], c='r', marker='o')
        plt.xlabel('Feature 1')
        plt.ylabel('Feature 2')
        plt.show()

    elif size == 3:
        from mpl_toolkits.mplot3d import Axes3D
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        # create graph
        for clid in np.unique(idx):
            ax.scatter(data[[no for no, i in enumerate(idx) if i == clid], 0],
                       data[[no for no, i in enumerate(idx) if i == clid], 1],
                       data[[no for no, i in enumerate(idx) if i == clid], 2], marker='^')
        # plot centers
        for clid in range(centers.shape[0]):
            plt.scatter(centers[clid, 0], centers[clid, 1], centers[clid, 2], c='r', marker='o')
        # set labels
        for i in [ax]:
            i.set_xlabel('Feature 1')
            i.set_ylabel('Feature 2')
            i.set_zlabel('Feature 3')

        plt.show()


