class TestCase:
    def __init__(self, name):
        self.name = name

    def setUp(self):
        pass

    def run(self):
        self.setUp()
        result = TestResult()
        result.testStarted()
        try:
            method = getattr(self, self.name)
            method()
        except:
            result.testFailed()

        return result


class TestResult:
    def __init__(self):
        self.runCount = 0
        self.errorCount = 0

    def summary(self):
        return "%d run, %d failed"%(self.runCount,self.errorCount)

    def testStarted(self):
        self.runCount += 1

    def testFailed(self):
        self.errorCount += 1


class Kmeans(TestCase):
    def setUp(self):
        pass

    def method(self):
        raise Exception

    def assignToCluster(self):
        """passed."""
        import numpy as np
        data = [np.array(range(10)), np.array(list(reversed(range(10))))]
        data = np.transpose(data)
        k = 3
        centers = data[[0, 1, 2], :]  # here we select k centroids
        idx = np.ones((data.shape[0], 1), dtype=int)

        from kmeans import _assignToCluster as assign
        import numpy as np
        assign(data, centers, idx, k)
        idx_true = np.transpose([np.array([0,1,2,2,2, 2,2,2,2,2])])
        assert np.array_equiv(idx_true, idx)

    def calculateClusterMeans(self):
        """passed."""
        from kmeans import calculateClusterMeans as calc
        import numpy as np
        data = np.transpose([np.array(range(1,4,1)), np.array(list(reversed(range(4,7,1))))])
        centers = np.transpose([np.array([0,0,0]), np.array([0,0,0])])
        idx = np.transpose([np.array([0, 1, 2])])
        calc(data, centers, idx)


        assert np.array_equiv(centers, data)


class TestCaseKmeans(TestCase):
    def testTryMethodFailed(self):
        test = Kmeans('method')
        result = test.run()
        assert result.summary() == "1 run, 1 failed"

    def test_assignToCluster(self):
        test = Kmeans('assignToCluster')
        result = test.run()
        assert result.summary() == "1 run, 0 failed"

    def test_calculateClusterMeans(self):
        test = Kmeans('calculateClusterMeans')
        result = test.run()
        assert result.summary() == "1 run, 0 failed"

    def test_set(self):
        pass



print(TestCaseKmeans('testTryMethodFailed').run().summary())
print(TestCaseKmeans('test_assignToCluster').run().summary())
print(TestCaseKmeans('test_calculateClusterMeans').run().summary())
